;; Copyright (c) 2021 Andrew Apted
;; Use of this code is governed by an MIT-style license.
;; See the accompanying "LICENSE.md" file for the full text.

#public

fun UTF8/decode (buf ^UChar len ssize ch ^UPoint -> ssize)
	; we don't care about UTF-16 surrogates here

	b   = [buf]
	res = conv UPoint b

	if eq? (iand b 0xF8) 0xF0
		; a four byte sequence
		jump invalid if lt? len 4

		res = iand res 0x07

		; first continuation byte
		buf = padd buf 1
		b   = [buf]

		jump invalid unless eq? (iand b 0xC0) 0x80

		b   = iand b   0x3F
		res = ishl res 6
		res = ior  res (conv UPoint b)

		; second continuation byte
		buf = padd buf 1
		b   = [buf]

		jump invalid unless eq? (iand b 0xC0) 0x80

		b   = iand b   0x3F
		res = ishl res 6
		res = ior  res (conv UPoint b)

		; third continuation byte
		buf = padd buf 1
		b   = [buf]

		jump invalid unless eq? (iand b 0xC0) 0x80

		b   = iand b   0x3F
		res = ishl res 6
		res = ior  res (conv UPoint b)

		; exclude overlong and invalid values
		jump invalid if lt? res 0x010000
		jump invalid if gt? res 0x10FFFF

		[ch] = res
		return 4

	elif eq? (iand b 0xF0) 0xE0
		; a three byte sequence
		jump invalid if lt? len 3

		res = iand res 0x0F

		; first continuation byte
		buf = padd buf 1
		b   = [buf]

		jump invalid unless eq? (iand b 0xC0) 0x80

		b   = iand b   0x3F
		res = ishl res 6
		res = ior  res (conv UPoint b)

		; second continuation byte
		buf = padd buf 1
		b   = [buf]

		jump invalid unless eq? (iand b 0xC0) 0x80

		b   = iand b   0x3F
		res = ishl res 6
		res = ior  res (conv UPoint b)

		; exclude overlong values
		jump invalid if lt? res 0x0800

		[ch] = res
		return 3

	elif eq? (iand b 0xE0) 0xC0
		; a two byte sequence, high + low
		jump invalid if lt? len 2

		res = iand res 0x1F

		; a single continuation byte
		buf = padd buf 1
		b   = [buf]

		jump invalid unless eq? (iand b 0xC0) 0x80

		b   = iand b   0x3F
		res = ishl res 6
		res = ior  res (conv UPoint b)

		; exclude overlong values
		jump invalid if lt? res 0x0080

		[ch] = res
		return 2

	else
		; bad sequence, e.g. an initial continuation byte
		jump invalid if ge? b 0x80

		; a single byte (7-bit ASCII)
		[ch] = res
		return 1
	endif

invalid:
	return -1
end


fun UTF8/encode (buf ^UChar ch UPoint -> ssize)
	; we don't care about UTF-16 surrogates here

	if gt? ch 0x10FFFF
		return -1

	elif gt? ch 0xFFFF
		; four bytes are required

		b  = ishr ch 18
		b  = iand b  0x07
		b  = ior  b  0xF0

		[buf] = conv UChar b
		buf   = padd buf 1

		b  = ishr ch 12
		b  = iand b  0x3F
		b  = ior  b  0x80

		[buf] = conv UChar b
		buf   = padd buf 1

		b  = ishr ch 6
		b  = iand b  0x3F
		b  = ior  b  0x80

		[buf] = conv UChar b
		buf   = padd buf 1

		b  = iand ch 0x3F
		b  = ior  b  0x80

		[buf] = conv UChar b
		return 4

	elif gt? ch 0x07FF
		; three bytes are required

		b  = ishr ch 12
		b  = iand b  0x0F
		b  = ior  b  0xE0

		[buf] = conv UChar b
		buf   = padd buf 1

		b  = ishr ch 6
		b  = iand b  0x3F
		b  = ior  b  0x80

		[buf] = conv UChar b
		buf   = padd buf 1

		b  = iand ch 0x3F
		b  = ior  b  0x80

		[buf] = conv UChar b
		return 3

	elif gt? ch 0x007F
		; two bytes: high and low

		b  = ishr ch 6
		b  = iand b  0x1F
		b  = ior  b  0xC0

		[buf] = conv UChar b
		buf   = padd buf 1

		b  = iand ch 0x3F
		b  = ior  b  0x80

		[buf] = conv UChar b
		return 2

	else
		; a single byte
		[buf] = conv UChar ch
		return 1
	endif
end


fun UTF16/decode (buf ^UWord len ssize ch ^UPoint -> ssize)
	high = conv UPoint [buf]

	if ge? high 0xDC00
		if le? high 0xDFFF
			; low surrogate without a high one
			return -1
		endif
	endif

	if ge? high 0xD800
		if le? high 0xDBFF
			if lt? len 2
				; high surrogate not followed by a low one
				return -1
			endif

			buf = padd buf 2
			low = conv UPoint [buf]

			if or (lt? low 0xDC00) (gt? low 0xDFFF)
				; high surrogate not followed by a low one
				return -1
			endif

			low  = iand low  0x03FF
			high = iand high 0x03FF
			high = ishl high 10
			high = iadd high low 0x10000

			[ch] = high
			return 2
		endif
	endif

	[ch] = high
	return 1
end


fun UTF16/encode (buf ^UWord ch UPoint -> ssize)
	if UCODE/surrogate? ch
		; leave it to caller to decide how to handle these
		return -1
	endif

	if lt? ch 0x10000
		[buf] = conv UWord ch
		return 1
	endif

	ch = isub ch 0x10000

	if ge? ch (1 << 20)
		return -1
	endif

	high  = ishr ch   10
	high  = ior  high 0xD800
	[buf] = conv UWord high

	buf   = padd buf 2

	low   = iand ch  0x03FF
	low   = ior  low 0xDC00
	[buf] = conv UWord low

	return 2
end

;------------------------------------------------------------------------

fun UTF8/valid? (buf ^UChar len ssize -> bool)
	ch = stack-var UPoint

	loop while some? len
		num = UTF8/decode buf len ch

		if neg? num
			return FALSE
		endif

		if UCODE/surrogate? [ch]
			return FALSE
		endif

		len = isub len num
		buf = padd buf num
	endloop

	return TRUE
end


fun UTF16/valid? (buf ^UWord len ssize -> bool)
	ch = stack-var UPoint

	loop while some? len
		num = UTF16/decode buf len ch

		if neg? num
			return FALSE
		endif

		len = isub len num
		num = ishl num 1
		buf = padd buf num
	endloop

	return TRUE
end


fun UTF32/valid? (buf ^UPoint len ssize -> bool)
	loop while some? len
		ch = [buf]

		if gt? ch 0x10FFFF
			return FALSE
		endif

		if UCODE/surrogate? ch
			return FALSE
		endif

		buf = padd buf 4
		len = isub len 1
	endloop

	return TRUE
end

;------------------------------------------------------------------------

;; NOTE:
;;     Windows apparently allows isolated surrogates in filenames.
;;     To allow these conversion functions to handle that, such
;;     surrogates are not treated as errors, but treated like a
;;     normal character.

fun UTF8/to-utf16 (in ^UChar len ssize out ^UWord maxout ssize -> ssize)
	total ssize = 0

	; account for surrogates
	maxout = isub maxout 1

	ch = stack-var UPoint

	loop while some? len
		break if ge? total maxout

		num = UTF8/decode in len ch

		; illegal sequences become an error character, and any
		; additional continuation bytes are skipped.
		if neg? num
			[out] = UPOINT_ERROR
			total = iadd total 1
			out   = padd out   2

			len = isub len 1
			in  = padd in  1

			loop while some? len
				b = iand [in] 0xC0
				break unless eq? b 0x80

				len = isub len 1
				in  = padd in  1
			endloop

			jump next
		endif

		len = isub len num
		in  = padd in  num

		; keep unpaired surrogates in the output
		if UCODE/surrogate? [ch]
			[out] = conv UWord [ch]
			num   = 1
		endif

		num = UTF16/encode out [ch]

		; unrepresentable values become an error character
		if neg? num
			[out] = UPOINT_ERROR
			num   = 1
		endif

		total = iadd total num
		num   = ishl num 1
		out   = padd out num

		next:
	endloop

	if ge? total maxout
		return -1
	endif

	[out] = 0
	return total
end


fun UTF8/to-utf32 (in ^UChar len ssize out ^UPoint maxout ssize -> ssize)
	total ssize = 0

	loop while some? len
		break if ge? total maxout

		num = UTF8/decode in len out

		; illegal sequences become an error character, and any
		; additional continuation bytes are skipped.
		if neg? num
			[out] = UPOINT_ERROR
			total = iadd total 1
			out   = padd out   4

			len = isub len 1
			in  = padd in  1

			loop while some? len
				b = iand [in] 0xC0
				break unless eq? b 0x80

				len = isub len 1
				in  = padd in  1
			endloop

			jump next
		endif

		total = iadd total 1
		out   = padd out   4

		len   = isub len num
		in    = padd in  num

		next:
	endloop

	if ge? total maxout
		return -1
	endif

	[out] = 0
	return total
end


fun UTF16/to-utf8 (in ^UWord len ssize out ^UChar maxout ssize -> ssize)
	total ssize = 0

	; account for longest UTF-8 sequence
	maxout = isub maxout 4

	ch = stack-var UPoint

	loop while some? len
		break if ge? total maxout

		num = UTF16/decode in len ch

		; keep unpaired surrogates in the output
		if neg? num
			[ch] = conv UPoint [in]
			num  = 1
		endif

		len = isub len num
		num = ishl num 1
		in  = padd in  num

		num = UTF8/encode out [ch]

		; unrepresentable values become an error character
		if neg? num
			num = UTF8/encode out UPOINT_ERROR
		endif

		total = iadd total num
		out   = padd out   num
	endloop

	if ge? total maxout
		return -1
	endif

	[out] = 0
	return total
end


fun UTF16/to-utf32 (in ^UWord len ssize out ^UPoint maxout ssize -> ssize)
	total ssize = 0

	loop while some? len
		break if ge? total maxout

		num = UTF16/decode in len out

		; keep unpaired surrogates in the output
		if neg? num
			[out] = conv UPoint [in]
			num   = 1
		endif

		len = isub len num
		num = ishl num 1
		in  = padd in  num

		total = iadd total 1
		out   = padd out   4
	endloop

	if ge? total maxout
		return -1
	endif

	[out] = 0
	return total
end


fun UTF32/to-utf8 (in ^UPoint len ssize out ^UChar maxout ssize -> ssize)
	total ssize = 0

	; account for longest UTF-8 sequence
	maxout = isub maxout 4

	loop while some? len
		break if ge? total maxout

		ch  = [in]
		in  = padd in 4
		len = isub len 1

		num = UTF8/encode out ch

		; unrepresentable values become an error character
		if neg? num
			num = UTF8/encode out UPOINT_ERROR
		endif

		total = iadd total num
		out   = padd out   num
	endloop

	if ge? total maxout
		return -1
	endif

	[out] = 0
	return total
end


fun UTF32/to-utf16 (in ^UPoint len ssize out ^UWord maxout ssize -> ssize)
	total ssize = 0

	; account for surrogates
	maxout = isub maxout 1

	loop while some? len
		break if ge? total maxout

		ch  = [in]
		in  = padd in 4
		len = isub len 1

		; keep unpaired surrogates in the output
		if UCODE/surrogate? ch
			[out] = conv UWord ch
			num ssize = 1
		else
			num = UTF16/encode out ch

			; unrepresentable values become an error character
			if neg? num
				[out] = UPOINT_ERROR
				num   = 1
			endif
		endif

		total = iadd total num
		num   = ishl num 1
		out   = padd out num
	endloop

	if ge? total maxout
		return -1
	endif

	[out] = 0
	return total
end
